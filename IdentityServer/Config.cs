﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApis()
        {
            return new ApiResource[]
            {
                new ApiResource("api1", "My API #1")
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            return new[]
            {
                // SPA client using Implicit flow
                new Client
                {
                    ClientId = "angular_spa",
                    ClientName = "SPA Client Implicit",

                    AllowedGrantTypes = GrantTypes.Implicit,

                    RequireClientSecret = false,

                    RedirectUris =
                    {
                        "http://localhost:4200/auth-callback"
                    },
                    PostLogoutRedirectUris = { "http://localhost:4200/" },
                    AllowAccessTokensViaBrowser = true,
                    AllowedScopes = { "openid", "profile", "api1" }
                },

                // SPA client using Code flow with PCKE
                new Client
                {
                    ClientId = "angular_spa1",
                    ClientName = "SPA Client Code with PKCE",

                    AllowedGrantTypes = GrantTypes.Code,
                    ClientSecrets = { new Secret("49C1A7E1-0C79-4A89-A3D6-A37998FB86B0".Sha256()) },
                    RequireClientSecret = true,
                    RequirePkce = true,

                    RedirectUris =
                    {
                        "http://localhost:4200/auth-callback"
                    },
                    PostLogoutRedirectUris = { "http://localhost:4200/" },
                    AllowAccessTokensViaBrowser =false,
                    AllowedScopes = { "openid", "profile", "api1" }
                }
            };
        }
    }
}