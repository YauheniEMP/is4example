import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient, private config: ConfigService) { }
  getprotected(token: string) {
    const httpOptions = {
      withCredentiald : true,
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: token
      })
    };
    const apiUri = this.config.resourceApiURI;
    return this.http.get(`${apiUri}/values/protected`, httpOptions);
  }
}
